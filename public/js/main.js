var swiper = new Swiper(".actors__slider", {
  slidesPerView: 'auto',
    spaceBetween: 24,
    loop: true,
   
    // pagination //
  
    pagination: {
      el: ".swiper-pagination",
    },
    
    // navigation //
    
    navigation: {
      nextEl: '.button__next',
      prevEl: '.button__prev',
    },
});
ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [55.751574, 37.573856],
            zoom: 12
        }, {
            searchControlProvider: 'yandex#search'
        }),

        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
        ),

        centerPlacemark = new ymaps.Placemark([55.7522, 37.6156], {
            hintContent: 'Магазин мерча ведьмака',
            balloonContent: 'Магазин мерча ведьмака',
        },
            {
            iconLayout: 'default#imageWithContent',
            iconImageHref: 'img/icon/point.svg',
            iconImageSize: [60, 60],
            iconImageOffset: [0, 0],
            iconContentOffset: [15, 15],
            iconContentLayout: MyIconContentLayout
        });
        
        horoshevskyPlacemark = new ymaps.Placemark([55.782294, 37.528208], {
            hintContent: 'Магазин мерча ведьмака',
            balloonContent: 'Магазин мерча ведьмака',
        },
            {
            iconLayout: 'default#imageWithContent',
            iconImageHref: 'img/icon/point.svg',
            iconImageSize: [60, 60],
            iconImageOffset: [0, 0],
            iconContentOffset: [15, 15],
            iconContentLayout: MyIconContentLayout
        });

        rochdelskayaPlacemark = new ymaps.Placemark([55.757106, 37.565111], {
            hintContent: 'Магазин мерча ведьмака',
            balloonContent: 'Магазин мерча ведьмака',
        },
            {
         
            iconLayout: 'default#imageWithContent',
            iconImageHref: 'img/icon/point.svg',
            iconImageSize: [60, 60],
            iconImageOffset: [0, 0],
            iconContentOffset: [15, 15],
            iconContentLayout: MyIconContentLayout
        });

    myMap.geoObjects
        .add(horoshevskyPlacemark)
        .add(rochdelskayaPlacemark)
        .add(centerPlacemark);
    });