var swiper = new Swiper(".actors__slider", {
  slidesPerView: 'auto',
    spaceBetween: 24,
    loop: true,
   
    // pagination //
  
    pagination: {
      el: ".swiper-pagination",
    },
    
    // navigation //
    
    navigation: {
      nextEl: '.button__next',
      prevEl: '.button__prev',
    },
});